package com.amdocs;

public class Increment {
	
	private int counter = 1;

	private int abc;
	  
	public int getCounter() {
	    return counter++;
	}
			
	public int decreaseCounter(final int input) {
    int result;

    if (input == 0) {
        result = counter--;
    } else if (input == 1) {
        result = counter;
    } else {
        result = counter++;
    }

    return result;
}	
}
